Template.welcome.onCreated(function (){
    Meteor.subscribe('posts');
});
Template.Welcome.events({
   "submit .name-form": function (event) {
      event.preventDefault();
       var name = event.target.firstname.value;
       
       Posts.insert({
            name: name
       });
   }
});